import { BrowserRouter, Route, Switch } from "react-router-dom";
import Footer from "./component/Footer";
import Home from "./component/Home";
import Navbar from "./component/Navbar";
import Absen from "./pages/Absen";
import EditList from "./pages/EditList";
import Login from "./pages/Login";
import Register from "./pages/Register";
import ToDoList from "./pages/ToDoList";
import Profil from "./pages/Profil";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <main>
          <Switch>
            <Route path="/register" component={Register} exact />
            <Route path="/login" component={Login} exact />
            <Route path="/edit/:id" component={EditList} exact />
            <Route path="/profile" component={Profil} exact />
            <div>
              <Navbar />
              <Route path="/" component={Home} exact />
              <Route path="/absen" component={Absen} exact />
              <Route path="/todolist" component={ToDoList} exact />
              <Footer />
            </div>
          </Switch>
        </main>
      </BrowserRouter>
    </div>
  );
}

export default App;
