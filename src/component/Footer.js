import React from "react";

export default function Footer() {
  return (
    <div className="px-16 py-8">
      <hr/>
      <p class="pt-4 text-center text-gray-500 text-xs">
        &copy;2023 PRELIST. Seluruh hak cipta.
      </p>
    </div>
  );
}
