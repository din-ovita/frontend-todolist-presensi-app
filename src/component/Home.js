import React from "react";
import list from "../image/undraw_my_documents_re_13dc.svg";

export default function Home() {
  return (
    <div
      className={`w-full h-screen pt-[18%] md:pt-[10%] lg:pt-[6%] px-8 md:px-10 lg:px-12`}
    >
      <div className="flex space-x-10 md:space-x-32 lg:space-x-64 ">
        <div className="w-auto pt-12 md:pt-28 lg:pt-32">
          <div className="">
            <div className="text-center">
              <h1 className="text-xl md:text-3xl lg:text-4xl max-w-[150px] lg:max-w-[500px] md:max-w-[400px] font-bold text-orange ">
                Atur rencanamu dan jangan lupa isi absennya!
              </h1>
            </div>
            <div className="mt-4 lg:mt-5 space-y-5 md:space-y-0 lg:space-y-0 md:flex lg:flex md:space-x-16 lg:space-x-6">
              <div className="border-4 rounded-md border-orange shadow-xl">
                <h1 className="text-base md:text-lg lg:text-xl font-semibold text-white bg-orange px-2 lg:py-1">
                  ABSENSI
                </h1>
                <h2 className="text-sm lg:text-base px-2 font-semibold lg:w-44">
                  Isi absensi mudah dengan sekali tekan
                </h2>
                <button className="bg-orange text-white border-2 border-orange px-2 py-1 rounded-md ml-32 mr-1 md:ml-32 lg:ml-48 md:mr-2 my-2">
                  <a href="/absen" className=" font-semibold hover:text-white ">
                    <i class="fa-solid fa-calendar-days"></i>
                  </a>
                </button>
              </div>
              <div className="border-4 rounded-md border-orange shadow-xl">
                <h1 className="text-base md:text-lg lg:text-xl font-semibold text-white bg-orange px-2 lg:py-1">
                  TO DO LIST
                </h1>
                <h2 className="text-sm lg:text-base px-2 font-semibold lg:w-44">
                  Atur rencana harianmu sekarang
                </h2>

                <button className="bg-white border-2 rounded-md border-orange  px-2 py-1 rounded-md ml-32 mr-1 md:ml-32 lg:ml-48 md:mr-2 my-2">
                  <a
                    href="/todolist"
                    className="font-semibold text-orange hover:text-orange"
                  >
                    <i class="fa-solid fa-list"></i>
                  </a>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div className="">
          <img src={list} className="mt-8 w-auto h-96 lg:mt-16" />
        </div>
      </div>
    </div>
  );
}
