import React from "react";
import { useHistory } from "react-router-dom";
import logo2 from "../image/pngegg (18).png";

export default function Navbar() {
  const history = useHistory();

  const logout = () => {
    window.location.reload();
    localStorage.clear();
    history.push("/login");
  };

  return (
    <div>
      <nav className="p-3 bg-white shadow flex items-center justify-between fixed w-full">
        <a href="/">
          <div className="flex justify-between items-center">
            <img src={logo2} className="h-10 mr-[5%] ml-2" />
            <span className="text-2xl font-semibold cursor-pointer hover:text-black">
              PRELIST
            </span>
          </div>
        </a>

        <ul className="flex">
          <li className="mx-2 my-2">
            <a
              href="/absen"
              className="text-base lg:text-lg md:text-lg hover:text-orange duration-500 font-medium text-zinc-600 md:mx-4 lg:mx-4"
            >
              Absensi
            </a>
          </li>
          <li className="mx-2 my-2">
            <a
              href="/todolist"
              className="text-base lg:text-lg md:text-lg hover:text-orange duration-500 font-medium text-zinc-600 md:mx-4 lg:mx-4"
            >
              Todolist
            </a>
          </li>
          <li className="mx-2 my-2">
            <a
              href="/profile"
              className="text-base lg:text-lg md:text-lg hover:text-orange duration-500 font-medium text-zinc-600 md:mx-4 lg:mx-4"
            >
              Profil
            </a>
          </li>
          <li>
            {localStorage.getItem("Id") !== null ? (
              <>
                <a onClick={logout}>
                  <button className="border-2 border-orange bg-white px-3 py-2 rounded-md font-semibold text-orange">
                    Log Out
                  </button>
                </a>
              </>
            ) : (
              <a href="/login">
                <button className="text-sm lg:text-lg md:text-lg bg-orange px-3 py-2 rounded-md font-semibold text-white">
                  Get Started
                </button>
              </a>
            )}
          </li>
        </ul>
      </nav>
    </div>
  );
}
