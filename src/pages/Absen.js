import axios from "axios";
import React, { useEffect, useState } from "react";
import ReactPaginate from "react-paginate";
import Swal from "sweetalert2";

export default function Absen() {
  const [absen, setAbsen] = useState([]);
  const [pages, setPages] = useState(0);

  // method add absen
  const addAbsen = async () => {
    const user = localStorage.getItem("Id");
    const result = await axios.post(`http://localhost:3012/absen/`, {
      userId: user,
      presensi: "MASUK",
    });
    sessionStorage.setItem("Absen", "Sudah");
    console.log(result.data.data);
    Swal.fire({
      icon: "success",
      title: "Absen Pagi Sukses!!",
      text: "Selamat Pagi!",
      showConfirmButton: false,
      timer: 1500,
    });
  };

  // method absen pulang
  const absenPulang = async () => {
    const user = localStorage.getItem("Id");
    const result = await axios.post(`http://localhost:3012/absen/`, {
      userId: user,
      presensi: "PULANG",
    });
    sessionStorage.setItem("Absen Pulang", "Sudah");
    console.log(result.data.data);
    Swal.fire({
      icon: "success",
      title: "Absen Pulang!!",
      text: "Selamat Istirahat di Rumah!",
      showConfirmButton: false,
      timer: 1500,
    });
  };

  // method getAll per Id
  const getAbsen = async (idx) => {
    try {
      const result = await axios.get(
        `http://localhost:3012/absen?page=${idx}&userId=${localStorage.getItem(
          "Id"
        )}`
      );
      console.log(result.data.data.content);
      setAbsen(result.data.data.content);
      setPages(result.data.data.totalPages);
    } catch (error) {
      console.log(error);
    }
  };

  // method delete
  const deleted = async (id) => {
    Swal.fire({
      title: "Kamu yakin ingin menghapus?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, hapus!",
    }).then(async (result) => {
      if (result.isConfirmed) {
        await axios.delete("http://localhost:3012/absen/" + id);
        Swal.fire("Terhapus!", "", "success");
        getAbsen(0);
      }
    });
  };

  useEffect(() => {
    getAbsen(0);
  }, []);

  return (
    <div className={"p-7 pt-[18%] md:pt-[10%] lg:pt-[8%]"}>
      <h1 className={`text-4xl font-bold flex-1`}>TABEL ABSENSI HARIAN </h1>
      <div className="flex justify-between items-center mt-5">
        <a onClick={addAbsen}>
          <div className="w-full bg-biru px-4 py-2 lg:px-5 lg:py-2 rounded cursor-pointer lg:w-[300px]">
            <div className="text-lg md:text-xl lg:text-2xl font-semibold text-gray-200">
              Absen Masuk
            </div>
            <div className="text-sm lg:text-base font-semibold text-slate-100">
              {sessionStorage.getItem("Absen") === null ? (
                <>Belum Absen</>
              ) : (
                <>Sudah Absen</>
              )}
            </div>
          </div>
        </a>

        <a onClick={absenPulang}>
        <div className="w-full bg-orange px-4 py-2 lg:px-5 lg:py-2 rounded cursor-pointer lg:w-[300px]">
        <div className="text-lg md:text-xl lg:text-2xl font-semibold text-gray-200">
              Absen Pulang
            </div>
            <div className="text-sm lg:text-base font-semibold text-slate-100">
              {sessionStorage.getItem("Absen Pulang") === null ? (
                <>Belum Absen</>
              ) : (
                <>Sudah Absen</>
              )}
            </div>
          </div>
        </a>
      </div>
      {/* TABEL */}
      <div className="container mx-auto">
        <div className="-mx-4 sm:-mx-8 px-4 sm:px-8 py-12 overflow-x-auto">
          <div className="inline-block min-w-full shadow-md rounded-lg overflow-hidden">
            <table className="min-w-full leading-normal ">
              <thead>
                <tr>
                  <th className="px-3 py-3 border-b-2 border-gray-200 bg-gray-50 text-left md:text-center text-base font-semibold text-orange uppercase tracking-wider">
                    KETERANGAN
                  </th>
                  <th className="px-3 py-3 border-b-2 border-gray-200 bg-gray-50 text-left md:text-center text-base font-semibold text-orange uppercase tracking-wider">
                    TANGGAL
                  </th>
                  <th className="px-3 py-3 border-b-2 text-center border-gray-200 bg-gray-50 text-left md:text-center text-base font-semibold text-orange uppercase tracking-wider">
                    JAM
                  </th>
                  <th className="px-3 py-3 border-b-2 border-gray-200 bg-gray-50 text-left md:text-center text-base font-semibold text-orange uppercase tracking-wider">
                    ACTION
                  </th>
                </tr>
              </thead>
              <tbody>
                {absen.map((absensi) => (
                  <tr>
                    <td className="px-3 py-2 bg-gray-200 text-sm md:text-center font-semibold text-gray-700">
                      {absensi.presensi}
                    </td>
                    <td className="px-3 py-2 bg-gray-200 text-sm md:text-center font-semibold text-gray-700">
                      {absensi.date}
                    </td>
                    <td className="px-3 py-2 bg-gray-200 text-sm md:text-center font-semibold text-gray-700">
                      {absensi.jam}
                    </td>
                    <td className="px-3 py-2 text-center bg-gray-200 text-sm md:text-center">
                      <button
                        className=" text-lg px-2 py-1 rounded text-red-600"
                        onClick={() => deleted(absensi.id)}
                      >
                        <i class="fa-solid fa-trash"></i>
                      </button>{" "}
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>

      {absen.length == 0 ? (
        <h5 align="center" className="my-5">
          Belum ada absensi
        </h5>
      ) : (
        <nav className="pb-10">
          <ReactPaginate
            previousLabel={"< Prev"}
            nextLabel={"Next >"}
            pageCount={pages}
            breakLabel={"..."}
            onPageChange={(e) => getAbsen(e.selected)}
            containerClassName={"pagination justify-content-center"}
            pageClassName={"page-item"}
            pageLinkClassName={"page-link"}
            previousLinkClassName={"page-link"}
            nextClassName={"page-item"}
            nextLinkClassName={"page-link"}
            activeClassName={"active"}
          />
        </nav>
      )}
    </div>
  );
}
