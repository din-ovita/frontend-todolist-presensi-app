import axios from "axios";
import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import Swal from "sweetalert2";

export default function EditList() {
  const param = useParams();
  const history = useHistory();
  const [task, setTask] = useState("");

  // method update
  const updateList = async (e) => {
    e.preventDefault();

    Swal.fire({
      title: "Yakin ingin memperbarui data?",
      text: "Data sebelumnya tidak bisa dikembalikan!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, Perbarui!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .put("http://localhost:3012/list/" + param.id, {
            task: task,
            userId: localStorage.getItem("Id"),
          })
          .then(() => {
            Swal.fire({
              icon: "success",
              title: "Sukses Memperbarui!",
              showConfirmButton: false,
              timer: 1500,
            });
            history.push("/todolist");
          })
          .catch((error) => {
            console.log(error);
          });
      }
    });
  };

  useEffect(() => {
    axios
      .get("http://localhost:3012/list/" + param.id)
      .then((response) => {
        const edit = response.data.data;
        setTask(edit.task);
      })
      .catch((error) => {
        alert("Terjadi kesalahan" + error);
        console.log(error);
      });
  }, []);

  return (
    <div className="h-screen">
      <div class="w-full max-w-xs lg:max-w-sm mx-auto pt-44">
        <form className="" onSubmit={updateList}>
          <div className="shadow-lg rounded-md px-3 md:px-4 py-4 md:w-10/12 bg-white">
            <h1 className="text-2xl lg:text-3xl font-semibold mb-3 text-center">
              Update List
            </h1>
            <div className="flex justify-betweeen items-center gap-9">
              <input
                id="task"
                type="text"
                value={task}
                onChange={(e) => setTask(e.target.value)}
                placeholder="Tambah todolist ... "
                className="shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-orange"
              />
              <button
                className="border-orange py-1 px-2 rounded bg-orange text-white mt-1"
                type="submit"
              >
                <i class="fa-solid fa-check"></i>
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}
