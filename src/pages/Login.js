import axios from "axios";
import logo2 from "../image/pngegg (18).png";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const history = useHistory();

  const login = async (e) => {
    e.preventDefault();

    try {
      const { data } = await axios.post(`http://localhost:3012/user/sign-in`, {
        email: email,
        password: password,
      });
      Swal.fire({
        icon: "success",
        title: "Berhasil Login",
        text: "Selamat datang di PreList",
        showConfirmButton: false,
        timer: 1500,
      });
      localStorage.setItem("Id", data.data.user.id);
      console.log(data);
      history.push("/");
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Login Gagal",
        text: "Username atau Password tidak valid",
        showConfirmButton: false,
        timer: 1500,
      });
      console.log(error);
    }
  };

  return (
    <div className="h-screen">
      <div className="flex justify-center items-center pt-16">
        <img src={logo2} className="h-10 lg:h-16" />
        <strong className=" text-2xl lg:text-4xl px-2 py-1 text-orange">
          PRELIST
        </strong>
      </div>
      <div class="w-full max-w-xs lg:max-w-sm mx-auto pt-12 pb-28">
        <form
          onSubmit={login}
          class="bg-white shadow-md rounded px-8 pt-1 pb-8 mb-4"
        >
          <h1 className="text-center text-2xl font-semibold">LOGIN</h1>
          <div class="mb-4 mt-9">
            <label
              class="block text-gray-700 text-sm font-bold mb-2"
              for="email"
            >
              Email
            </label>
            <input
              class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-orange "
              id="email"
              type="text"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              placeholder="Enter your email ..."
            />
          </div>
          <div class="mb-6">
            <label
              class="block text-gray-700 text-sm font-bold mb-2"
              for="password"
            >
              Password
            </label>
            <input
              class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-orange focus:shadow-outline"
              id="password"
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              placeholder="******************"
            />
          </div>
          <div class="flex items-center justify-between">
            <button
              class="bg-orange hover:bg-orange2 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
              type="submit"
            >
              Sign In
            </button>
            <a
              class="inline-block align-baseline font-bold text-sm text-orange hover:text-orange2"
              href="/register"
            >
              Buat akun
            </a>
          </div>
        </form>
        <p class="text-center text-gray-500 text-xs">
          &copy;2023 PRELIST. Seluruh hak cipta.
        </p>
      </div>
    </div>
  );
}
