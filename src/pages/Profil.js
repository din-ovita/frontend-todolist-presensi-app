import axios from "axios";
import React, { useEffect, useState } from "react";
import { Form, Modal } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import Navbar from "../component/Navbar";

export default function Profil() {
  const [user, setUser] = useState([]);
  const history = useHistory();
  const [show, setShow] = useState(false);

  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [bio, setBio] = useState("");
  const [noHp, setNoHp] = useState(0);
  const [profil, setProfile] = useState(null);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const getById = async () => {
    axios
      .get(`http://localhost:3012/user/` + localStorage.getItem("Id"))
      .then((res) => {
        console.log(res.data);
        setUser(res.data);
      });
  };

  const updateProfil = async (e) => {
    e.persist();
    e.preventDefault();
    const formData = new FormData();
    formData.append("file", profil);
    formData.append("email", email);
    formData.append("password", password);
    formData.append("username", username);
    formData.append("bio", bio);
    formData.append("noHp", noHp);
    Swal.fire({
      title: "Yakin ingin memperbarui data?",
      text: "Data sebelumnya tidak bisa dikembalikan!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, Perbarui!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .put(
            "http://localhost:3012/user/" + localStorage.getItem("Id"),
            formData
          )
          .then(() => {
            history.push("/profile");
            Swal.fire("Berhasil", "Data kamu berhasil di update", "success");
          })
          .catch((err) => {
            console.log(err);
          });
      }
    });
  };

  useEffect(() => {
    axios
      .get("http://localhost:3012/user/" + localStorage.getItem("Id"))
      .then((res) => {
        const edit = res.data;
        setUsername(edit.username);
        setBio(edit.bio);
        setEmail(edit.email);
        setPassword(edit.password);
        setNoHp(edit.noHp);
        setProfile(edit.profil);
      })
      .catch((error) => {
        console.log(error);
      });
    getById();
  }, []);

  return (
    <div>
      <Navbar />
      <img
        src={"https://wallpaper.dog/large/10938325.png"}
        className="w-full h-screen"
      />
      <div className="ml-[17%] h-[60%] w-[65%] md:w-[50%] lg:w-[50%] md:ml-[25%] lg:ml-[25%] bg-white absolute bottom-0 rounded-t-3xl">
        <img
          className="w-56 h-56 md:w-64 md:h-64 lg:w-64 lg:h-64 rounded-full absolute -right-16 -top-16 md:-right-24 md:-top-24 lg:-right-24 lg:-top-24 border-8 border-white"
          src={
            !user.profil
              ? "https://i.pinimg.com/236x/ce/b7/31/ceb731ddfc47f630168f1798a1c09e48.jpg"
              : user.profil
          }
        />
        <div className="text-center mt-[50%] md:mt-[8%] lg:mt-[8%] font-semibold">
          <p className="mt-2 text-2xl md:text-3xl lg:text-4xl">{user.username}</p>
          <p className="mt-2 text-sm md:text-base lg:text-base text-slate-600">{user.email}</p>
          <p className="mt-1 text-sm md:text-base lg:text-base text-slate-600">{user.noHp}</p>
        </div>

        <div className="text-center mt-9 md:mt-16 lg:mt-16 font-semibold text-lg">
          <p>{!user.bio ? <>Tambahkan Bio Anda</> : user.bio}</p>
        </div>
        <div className="mt-[10%] ml-[10%] pb-[2%]">
          <button
            onClick={handleShow}
            type="button"
            className="text-sm lg:text-lg md:text-lg bg-orange px-3 py-2 rounded-md font-semibold text-white"
          >
            EDIT PROFIL
          </button>
        </div>
      </div>

      <div>
        <Modal show={show} onHide={handleClose}>
          <Modal.Header className="bg-orange text-white uppercase" closeButton>
            <Modal.Title>Edit Profile</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form onSubmit={updateProfil}>
              <div class="mb-4">
                <label class="block text-gray-700 text-sm font-bold mb-2">
                  Foto Profile
                </label>
                <input
                  class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  onChange={(e) => setProfile(e.target.files[0])}
                  type="file"
                />
              </div>
              <div class="mb-4">
                <label class="block text-gray-700 text-sm font-bold mb-2">
                  Username
                </label>
                <input
                  class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  type="nama"
                  value={username}
                  onChange={(e) => setUsername(e.target.value)}
                />
              </div>
              <div class="mb-4">
                <label class="block text-gray-700 text-sm font-bold mb-2">
                  Email
                </label>
                <input
                  class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  type="email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
              </div>
              <div class="mb-4">
                <label class="block text-gray-700 text-sm font-bold mb-2">
                  Bio
                </label>
                <input
                  class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  type="text"
                  value={bio}
                  onChange={(e) => setBio(e.target.value)}
                />
              </div>

              <div class="mb-4">
                <label class="block text-gray-700 text-sm font-bold mb-2">
                  No Telpon
                </label>
                <input
                  class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  type="telepon"
                  value={noHp}
                  onChange={(e) => setNoHp(e.target.value)}
                />
              </div>
              <div class="mb-6">
                <label class="block text-gray-700 text-sm font-bold mb-2">
                  Password
                </label>
                <input
                  class="shadow appearance-none border border-red-500 rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                  type="password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
              </div>
              <button
                type="button"
                className="bg-red-600 text-lg px-2 py-1 rounded text-white mr-5"
                onClick={handleClose}
              >
                Close
              </button>
              <button className="btn btn-primary" onClick={handleClose}>
                Save
              </button>
            </Form>
          </Modal.Body>
        </Modal>
      </div>
    </div>
  );
}
