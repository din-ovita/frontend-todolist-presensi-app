import axios from "axios";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import todolist from "../image/undraw_to_do_re_jaef.svg";

export default function ToDoList() {
  const [task, setTask] = useState("");
  const [list, setList] = useState([]);

  const history = useHistory();

  // method post
  const addTask = async (tugas) => {
    console.log(tugas);
    const user = localStorage.getItem("Id");
    const result = await axios.post(`http://localhost:3012/list/`, {
      userId: user,
      task: task,
    });
    console.log(result.data.data);
    Swal.fire({
      icon: "success",
      title: "List Ditambahkan!",
      showConfirmButton: false,
      timer: 1500,
    });
  };

  // method get
  const getTask = async () => {
    try {
      const resp = await axios.get(
        `http://localhost:3012/list?userId=${localStorage.getItem("Id")}`
      );
      console.log(resp);
      setList(resp.data.data);
    } catch (error) {
      console.log(error);
    }
  };

  // method delete
  const deleteTask = async (id) => {
    Swal.fire({
      title: "Kamu yakin ingin menghapus?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, hapus!",
    }).then(async (result) => {
      if (result.isConfirmed) {
        await axios.delete("http://localhost:3012/list/" + id);
        Swal.fire("Terhapus!", "", "success");
        getTask();
      }
    });
  };

  // const selesai
  const validasi = async (id) => {
    try {
      await axios.put(`http://localhost:3012/list/selesai/` + id);
      Swal.fire({
        icon: "success",
        title: "Selesai!",
        showConfirmButton: false,
        timer: 1500,
      });
      history.push("/todolist");
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getTask();
  }, []);

  return (
    <div className={"p-7 pt-[18%] md:pt-[11%] lg:pt-[9%]"}>
      <h1 className="font-bold text-4xl lg:text-5xl text-center mt-4">
        ToDoList
      </h1>
      <div className="block md:flex lg:flex md:space-x-20 lg:space-x-64">
        <img
          src={todolist}
          className="w-96 md:w-96 lg:w-4/12 h-96 mt-8 lg:mt-10 lg:mx-12"
        />

        <form className=" mt-4 mb-9 lg:mt-20 md:mt-20 " onSubmit={addTask}>
          <div className="shadow-md rounded-md px-5 py-4 mx-4 md:my-8 lg:my-32 w-auto md:w-10/12 lg:w-10/12">
            <h1 className="text-2xl lg:text-3xl font-semibold mb-3 text-center">
              Tambah List
            </h1>
            <div className="flex justify-betweeen items-center gap-9">
              <input
                id="task"
                type="text"
                value={task}
                onChange={(e) => setTask(e.target.value)}
                placeholder="Tambah todolist ... "
                className="shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-orange"
              />
              <button
                className="border-orange py-1 px-2 rounded bg-orange text-white mt-1"
                type="submit"
              >
                <i class="fa-solid fa-plus"></i>
              </button>
            </div>
          </div>
        </form>
      </div>
      <div className="pb-10 px-10 md:px-12 lg:px-20 ">
        <div className="mx-2 flex flex-wrap">
          {list.map((tugas) => (
            <div
              className="border-2 border-orange rounded-md my-2 ml-4 md:ml-6 lg:ml-8 w-80"
              key={tugas.id}
            >
              <div className="flex justify-between items-center bg-orange">
                <p className="px-3 py-1 font-semibold text-white">
                  {tugas.status}
                </p>
                <p className="py-1 pr-3 font-semibold text-white">
                  {tugas.createdAt}
                </p>
              </div>
              <p className="px-3 py-2 font-medium text-lg">{tugas.task}</p>

              <div className="flex space-x-2 px-3 py-2">
                <button
                  className=" ml-[60%] bg-green-600 text-lg px-2 py-1 rounded text-white"
                  onClick={() => validasi(tugas.id)}
                >
                  <i class="fa-solid fa-check"></i>
                </button>
                <a href={"/edit/" + tugas.id}>
                  <button className="bg-orange text-lg px-2 py-1 rounded text-white">
                    <i class="fa-solid fa-pen-to-square"></i>
                  </button>
                </a>
                <button
                  onClick={() => deleteTask(tugas.id)}
                  className="bg-red-600 text-lg px-2 py-1 rounded text-white"
                >
                  <i class="fa-solid fa-trash"></i>
                </button>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
