/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{jsx,ts,js,tsx}"],
    theme: {
    extend: {
      colors: {
        clifford: "#da373d",
        orange: "#ED7014",
        pastel: "#ff9b57",
        krem: "#fffdc7",
        lgwhite: "rgba(255, 255, 255, 0.17)",
      },
      // dropShadow: {
      //   '3xl': '0 35px 35px rgb(237, 112, 20)',
      //   '4xl': [
      //       '0 35px 35px rgba(0, 0, 0, 0.25)',
      //       '0 45px 65px rgba(0, 0, 0, 0.15)'
      //   ]
      // }
      // screens: {
      //   sm: "480px",
      //   md: "600px",
      //   lg: "800px",
      //   xl: "900px",
      // },
    },
  },
  plugins: [require('@tailwindcss/forms')],
}